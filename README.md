**Запуск**
 * после распаковки сделать `composer install `
 * прописать свои конфиги базы в файле `.env` 
 * применить миграцию командой `php bin/console orm:schema-tool:update`
 * загрузить трейлеры командой `php bin/console fetch:trailers`
 